%global debug_package %{nil}

Name:    unixbench
Version: 5.1.3
Release: 3
Summary: UnixBench is the original BYTE UNIX benchmark suite, updated and revised by many people over the years
License: GPLv2
URL:	 https://github.com/kdlucas/byte-unixbench
Source0: https://github.com/kdlucas/byte-unixbench/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
Patch0: unixbench-add-HOWTO.patch

BuildRequires:  gcc

%description
The purpose of UnixBench is to provide a basic indicator of the performance of a Unix-like system; hence, multiple tests are used to test various aspects of the system's performance. These test results are then compared to the scores from a baseline system to produce an index value, which is generally easier to handle than the raw scores. The entire set of index values is then combined to make an overall index for the system

For the reason that this is not a packaging friendly software, please refer to HOWTO to find out how to use this package


%prep
%setup -q -n byte-%{name}-%{version}/
%patch0 -p1

%build
cd UnixBench
%make_build

%install
cd UnixBench
mkdir -p %{buildroot}/opt/%{name}/pgms
mkdir -p %{buildroot}/opt/%{name}/src
mkdir -p %{buildroot}/opt/%{name}/testdir

install -m 0644 src/* %{buildroot}/opt/%{name}/src/
install -m 0644 testdir/* %{buildroot}/opt/%{name}/testdir/
install -m 0644 README %{buildroot}/opt/%{name}/
install -m 0644 USAGE %{buildroot}/opt/%{name}/
install -m 0644 HOWTO %{buildroot}/opt/%{name}/
install -m 0644 Makefile %{buildroot}/opt/%{name}/
install -m 0755 Run %{buildroot}/opt/%{name}/
install -m 0755 pgms/gfx-x11 %{buildroot}/opt/%{name}/pgms/
install -m 0755 pgms/multi.sh %{buildroot}/opt/%{name}/pgms/
install -m 0755 pgms/*.sh %{buildroot}/opt/%{name}/pgms/
install -m 0644 pgms/index.base %{buildroot}/opt/%{name}/pgms/
install -m 0644 pgms/unixbench.logo %{buildroot}/opt/%{name}/pgms/

%pre
%preun
%post
%postun

%check

%files
/opt/%{name}/*

%changelog
* Thu May 6 2021 ssstone <31761424@qq.com> - 5.1.3-3
- fix ./Run shell1 excute err

* Wed Sep 16 2020 ShiYuan Hu <421699196@qq.com> - 5.1.3-2
- refresh source0 url

* Sun Mar 29 2020 Wei Xiong <myeuler@163.com>
- Package init

